import sbt._

object Dependencies {
  private val CirceVersion      = "0.14.6"
  private val MonocleVersion    = "3.2.0"
  private val PureconfigVersion = "0.17.4"
  private val TapirVersion      = "1.7.3"

  lazy val cats = Seq(
    "org.typelevel" %% "cats-core"   % "2.10.0",
    "org.typelevel" %% "cats-effect" % "3.5.1"
  )

  lazy val fs2 = Seq(
    "co.fs2" %% "fs2-core" % "3.9.1"
  )

  lazy val httpServer = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-core"              % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe"        % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server"     % TapirVersion,
    "org.http4s"                  %% "http4s-blaze-server"     % "0.23.15"
  )

  lazy val json = Seq(
    "io.circe" %% "circe-core"    % CirceVersion,
    "io.circe" %% "circe-generic" % CirceVersion,
    "io.circe" %% "circe-parser"  % CirceVersion
  )

  lazy val lens = Seq(
    "dev.optics" %% "monocle-core"  % MonocleVersion,
    "dev.optics" %% "monocle-macro" % MonocleVersion
  )

  lazy val utils = Seq(
    // Logging
    "com.typesafe.scala-logging" %% "scala-logging"   % "3.9.5",
    "ch.qos.logback"             %  "logback-classic" % "1.2.11",
    // Config parsing
    "com.github.pureconfig" %% "pureconfig"             % PureconfigVersion,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % PureconfigVersion,
    // Dependency injection
    "com.softwaremill.macwire" %% "macros" % "2.5.9" % "provided"
  )
}

