package scheduler.ping

import scheduler.ApiDefinition
import scheduler.ApiImplementation
import cats.effect.IO
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import scheduler.ApiError
import scheduler.BadRequest

case class MathInput(x: Int, y: Int) {
  lazy val div: Either[ApiError, MathOutput] =
    if (y == 0)
      Left(BadRequest("Pick literally any other integer for the dividend."))
    else Right(MathOutput(x / y))
  lazy val sum = MathOutput(x + y)
}

case class MathOutput(result: Int)

object PingApi extends ApiDefinition {
  val divide = baseEndpoint
    .in("div")
    .in(jsonBody[MathInput])
    .post
    .out(jsonBody[MathOutput])
  val echo = baseEndpoint.in("echo" / path[String]("stuff")).get.out(stringBody)
  val ping = baseEndpoint.in("ping").get.out(stringBody)
  val sum = baseEndpoint
    .in("sum")
    .in(jsonBody[MathInput])
    .post
    .out(jsonBody[MathOutput])
}

class PingApi extends ApiImplementation {
  val endpoints: List[ServerEndpoint[Any, IO]] = List(
    PingApi.divide.serverLogic(divLogic),
    PingApi.echo.serverLogicSuccess(echoLogic),
    PingApi.ping.serverLogicSuccess(pingLogic),
    PingApi.sum.serverLogicSuccess(sumLogin)
  )

  def divLogic(input: MathInput): IO[Either[ApiError, MathOutput]] =
    IO(input.div)
  def echoLogic(input: String): IO[String] = IO(input)
  def pingLogic(u: Unit): IO[String] = IO("pong")
  def sumLogin(input: MathInput): IO[MathOutput] = IO(input.sum)
}
