package scheduler

import cats.effect.IO
import io.circe.generic.AutoDerivation
import sttp.model.StatusCode
import sttp.tapir.{Tapir, PublicEndpoint}
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.TapirJsonCirce
import sttp.tapir.server.ServerEndpoint

trait ApiDefinition extends Tapir with TapirJsonCirce with AutoDerivation {
	protected val baseEndpoint: PublicEndpoint[Unit, ApiError, Unit, Any] =
		endpoint.errorOut(
			oneOf[ApiError](
				oneOfVariant(StatusCode.BadRequest, jsonBody[BadRequest]),
				oneOfVariant(StatusCode.NotFound, jsonBody[NotFound])
			)
		)
}

trait ApiImplementation {
	val endpoints: List[ServerEndpoint[Any, IO]]
}

trait DocumentedApi {
	val endpoints: List[ServerEndpoint[Any, IO]]
}

