package scheduler

sealed trait ApiError

case class BadRequest(message: String) extends ApiError
case class NotFound() extends  ApiError
