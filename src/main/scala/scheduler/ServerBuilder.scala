package scheduler

import cats.effect.kernel.Resource
import cats.effect.IO
import cats.implicits._
import org.http4s.server.Server
import org.http4s.HttpRoutes
import sttp.tapir.server.http4s.Http4sServerInterpreter
import org.http4s.server.Router
import org.http4s.blaze.server.BlazeServerBuilder

trait ServerConfig {
	val host: String
	val port: Int
}

object ServerBuilder {
	def apply(config: ServerConfig, apis: List[ApiImplementation]): Resource[IO, Server] = {
		val routes: HttpRoutes[IO] = Http4sServerInterpreter[IO]().toRoutes(apis.flatMap(_.endpoints))
		val router = Router(
			"/" -> routes
		)

		BlazeServerBuilder[IO]
			.bindHttp(host = config.host, port = config.port)
			.withHttpApp(router.orNotFound)
			.resource
	}
}
