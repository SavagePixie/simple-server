package scheduler

import cats.effect.{ExitCode, IO, IOApp}
import cats.effect.kernel.Resource
import com.softwaremill.macwire.{wire, wireSet}
import scheduler.ping.PingApi

case class AppConfig(host: String, port: Int) extends ServerConfig

object Main extends IOApp {
	lazy val resources = for {
		config <- loadConfig
		module = new ServerModule(config)
		_ <- module.server
	} yield ExitCode.Success

	def loadConfig: Resource[IO, AppConfig] = Resource.eval(IO(AppConfig("localhost", 7000)))

	def run(args: List[String]): IO[ExitCode] = resources.useForever
}

class ServerModule(config: AppConfig) {
	lazy val pingApi: PingApi = wire[PingApi]
	lazy val apis: List[ApiImplementation] = wireSet[ApiImplementation].toList
	lazy val server = ServerBuilder(config, apis)
}
